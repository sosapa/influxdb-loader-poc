# influxdb-loader-poc

## Description

A quick and dirty data loader from a CSV file over to influxDB installed on a given server.


## How to run

To run this put csv file with timestamp, route and elapsed on the data folder with name like elapsed_<date>.csv
and then do 

```gw bootRun```

Alternatively check 'How to use it'  section

ctrl+C to stop and verify files have been moved out of the data folder.

## Influx

### Querying

Command line connection to influx DB

```
> influx -host $INFLUXDB_HOST

Connected to http://10.92.8.23:8086 version 1.7.8
InfluxDB shell version: v1.7.7

>
```

show databases
```
> show databases  

name
----
_internal
telegraf
applications

# use one

> use applications
```

check retention policies
```
> show retention policies

name               duration   shardGroupDuration replicaN default
----               --------   ------------------ -------- -------
autogen            0s         168h0m0s           1        false
RedRouterRetention 17520h0m0s 1h0m0s             5        true

```

query the DB with:

```
> SELECT * FROM redrouter_audit
```      

and should show the data loaded


### How to run on Docker

```docker run -d -p 8086:8086 \                                                                                                                                                                                                                      <<<
         -v $PWD:/var/lib/influxdb \
         influxdb
```


### Influx DB creation

```curl -i -XPOST http://localhost:8086/query --data-urlencode "q=CREATE DATABASE redrouter_audit"```

### Influx Retention Policy

```curl -i -XPOST http://$INFLUXDB_HOST:${INFLUXDB_PORT}/query --data-urlencode "q=CREATE RETENTION POLICY RedRouterRetention ON redrouter_audit DURATION 720d REPLICATION 1 SHARD DURATION 30m DEFAULT"```

### DML Insert row

```curl -i -XPOST 'http://localhost:8086/write?db=redrouter_audit' --data-binary 'route_audit,host=server02,route=nswgas_market_data_outbox value=0.51'```

### CLI tool 

There is a CLI tool for influxDB called influx can be installed with brew (or whatever package manager)

``` brew install influxdb```

In there you can run queries and admin commands

[Influx DB](https://docs.influxdata.com/influxdb/v1.7/introduction/getting-started/)

         
## How to run Grafana

[Grafana has API](http://docs.grafana.org/http_api/) 

``` docker run -d --name=grafana -p 3000:3000 grafana/grafana```


## This app - Influx Loader
 
### How to use it
Besides the bootRun, the environment can be set up as described below. This process allows the data extraction from 
the logs and subsequent upload to Influx. The Grafana at this stage has to be done from the Browser, however is 
possible (I think) to use REST to setup 

### Building Spring app
```gw assemble```

### Bash environment setup
 
Alias and functions on bash
Add this to your bash to be able to invoke the loader app
```bash 
function influxl() {
  java -jar $INFLUXDB_LOADER_POC_HOME/build/libs/influxdb-loader-poc.jar $@
}
```       
### Data generation

```bash    

# Influx loader
export INFLUXDB_LOADER_POC_HOME=$RED/influxdb-loader-poc
export INFLUXDB_RETENTION_DAYS=730
export INFLUXDB_DATABASE_INSTANCES=5
export INFLUXDB_SHARD_DURATION=30

# Spring - Influx DB POC loader

export GENTRACK_DATASOURCE_URL="jdbc:oracle:thin:@//gta_p.redenergy.com.au:1541/GTA_P.redenergy.com.au"
export GENTRACK_DATASOURCE_USERNAME="genreduser"
export GENTRACK_DATASOURCE_PASSWORD="???"
export REDX_DATASOURCE_URL="jdbc:oracle:thin:@//redx_p.redenergy.com.au:1583/REDX_P.redenergy.com.au"
export REDX_DATASOURCE_USERNAME="redxread"
export REDX_DATASOURCE_PASSWORD="????"

function retrieveStats() {
  export targetDate=$1
  export startInfluxLoader=$2
  if ! [ -z "$targetDate" ]; then
    echo "Generate in remote for $targetDate"
    ssh tomcat_jasp "~/bin/generateStats.sh $targetDate"
  else
    export yesterday=$(gdate -d "yesterday 13:00" '+%Y-%m-%d')
    echo "Generate in remote for $yesterday"
    ssh tomcat_jasp "~/bin/generateStats.sh $yesterday"
    export today=$(gdate  '+%Y-%m-%d')
    echo "Generate in remote for $today"
    ssh tomcat_jasp "~/bin/generateStats.sh $today"
  fi

  for each in $(ssh tomcat_jasp 'ls  /tmp/e*_2019-0*.csv'); do
    echo $each
    scp tomcat_jasp:$each .
    echo "Remove remote"
    ssh tomcat_jasp "rm $each"
    mv $(basename $each) data
  done

  echo "Staring influx [$startInfluxLoader]?"
  if ! [ -z "$startInfluxLoader" ]; then
    echo "Running influx db loader next"
    influxl &
  else
    echo "Completed"
  fi
}            

a

function retrieveRedconStats() {
  export targetDate=$1
  export startInfluxLoader=$2
  if ! [ -z "$targetDate" ]; then
    echo "Generate in remote for $targetDate"
    ssh tomcat_jasp2 "~/bin/generateRedconError.sh $targetDate"
  else
    export yesterday=$(gdate -d "yesterday 13:00" '+%Y-%m-%d')
    echo "Generate in remote for $yesterday"
    ssh tomcat_jasp2 "~/bin/generateRedconError.sh $yesterday"
    export today=$(gdate  '+%Y-%m-%d')
    echo "Generate in remote for $today"
    ssh tomcat_jasp2 "~/bin/generateRedconError.sh $today"
  fi

  for each in $(ssh tomcat_jasp2 'ls  /tmp/redcon_e*_2019-0*.csv'); do
    echo $each
    scp tomcat_jasp2:$each .
    echo "Remove remote"
    ssh tomcat_jasp2 "rm $each"
    mv $(basename $each) data
  done

  echo "Staring influx [$startInfluxLoader]?"
  if ! [ -z "$startInfluxLoader" ]; then
    echo "Running influx db loader next, TODO"
    #influxl &
  else
    echo "Completed"
  fi
}
```                        

Note tomcat_jasp and tomcat_jasp2 are .ssh/config entries with public key installed

```bash 
Host = tomcat_jasp
HostName = 192.168.206.130
#HostName = red-ml3jasp.redenergy.com.au
User = tomcat
IdentityFile = ~/.ssh/your_key_file_ideally_following_ssh_standard_practices

Host = tomcat_jasp2
HostName = red-ml3jasp2.redenergy.com.au
User = tomcat
IdentityFile = ~/.ssh/your_key_file_ideally_following_ssh_standard_practices
```



