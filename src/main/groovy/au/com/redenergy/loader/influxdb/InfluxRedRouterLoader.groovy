package au.com.redenergy.loader.influxdb

import groovy.sql.Sql
import groovy.util.logging.Slf4j
import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import org.influxdb.dto.Point
import org.influxdb.dto.Query
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct
import javax.annotation.Resource
import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.chrono.IsoChronology
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.format.ResolverStyle
import java.util.concurrent.TimeUnit
/**
 * Reference: https://github.com/influxdata/influxdb-java
 */
@Component("dataLoader")
@Slf4j
class InfluxRedRouterLoader {

  static List<Map> processed = []

  @Value('${influxdb.host}')
  String host

  @Value('${influxdb.port}')
  String port

  @Value('${influxdb.database}')
  String databaseName

  @Value('${influxdb.username}')
  String username

  @Value('${influxdb.password}')
  String password

  @Value('${influxdb.retentionPolicy}')
  String retentionPolicy

  @Value('${influxdb.datapath}')
  String dataPath

  @Value('${influxdb.tableName}')
  String table

  @Value('${influxdb.retention.days}')
  String retentionDays

  @Value('${influxdb.shard.duration}')
  String shardDuration

  @Value('${influxdb.database.instances}')
  Integer databaseInstances

  @Value('${influxdb.database.queryDays}')
  Integer databaseQueryDays

  @Resource
  Sql groovySql

  @Resource
  Sql redxGroovySql

  InfluxDB client

  InfluxDB createClient() {
    InfluxDBFactory.connect("http://${host}:${port}", username, password)
  }

  @PostConstruct
  void init() {
    client = createClient()
    client.query(new Query("CREATE RETENTION POLICY ${retentionPolicy} ON ${databaseName} DURATION ${retentionDays} REPLICATION ${databaseInstances} SHARD DURATION ${shardDuration} DEFAULT", "", true))
    client.enableBatch()
    run()
  }

  void run() {
    loadElapsed()
    loadErrors()
    loadErrors("redcon", "redx", "wayne", "candi", "prat",
        "amiviewer",
        "bandit-webapp",
        "billinfo-service",
        "billing-system-service",
        "candi",
        "coucou",
        "curro",
        "dbdiff-scheduled",
        "ddebittracker",
        "digital-services",
        "epis",
        "flexComparator",
        "gascon-webapp",
        "holdback-service",
        "mirner_service",
        "mover-service",
        "pdfpitt-service",
        "pdfpitt-website",
        "prat-webapp",
        "qantas-service",
        "redcross",
        "retail-info-service",
        "site-role-version-service",
        "switchtracker",
        "voodoo-webapp"
    )
    loadMatchedDataFromDb()
    loadInboundFromRedxDb()
  }

  void loadElapsed() {
    Path allFiles = Paths.get(dataPath)
    List<File> dataFiles = allFiles.toFile().listFiles().findAll { File file -> file.name.startsWith("elapsed") }
    dataFiles.each {File file ->
      log.info("Loding file: ${file}")
      loadElapsedFile(file)
    }
  }

  void loadErrors() {
    Path allFiles = Paths.get(dataPath)
    List<File> dataFiles = allFiles.toFile().listFiles().findAll { File file -> file.name.startsWith("errors") }
    dataFiles.each {File file ->
      log.info("Loding file: ${file}")
      loadErrorsFile(file)
    }
  }

  void loadErrors(String ... apps) {
    Path allFiles = Paths.get(dataPath)
    apps.each{ String app ->
      List<File> dataFiles = allFiles.toFile().listFiles().findAll { File file -> file.name.startsWith("${app}_errors") }
      dataFiles.each {File file ->
        log.info("Loding ${app} file: ${file}")
        loadErrorsFile(app, file)
      }
    }
  }

  void loadElapsedFile(File file) {
    List<String> lines = file.readLines()
    lines.each { String line ->
      String[] values = line.split(",")
      String timeAsString = values[0].trim().replace(" ", "T")
      LocalDateTime eventTimestamp = LocalDateTime.parse(timeAsString, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
      ZonedDateTime zonedDateTime = eventTimestamp.atZone(ZoneOffset.systemDefault())
      Long millis = zonedDateTime.toInstant().toEpochMilli()
      Long elapsed = Long.valueOf(values[1].trim())
      String route = values[2].trim()
      Long size = (values[3].trim().replaceAll("null","0"))?Long.valueOf(values[3].trim().replaceAll("null","0")):0

      log.info("Processing [$eventTimestamp($millis),$elapsed,$route]")
      Point point = Point.measurement("route_audit")
          .tag("route", route)
          .addField("elapsed", elapsed)
          .addField("exchange_size", size)
          .time(millis, TimeUnit.MILLISECONDS)
          .build()

      client.write(databaseName, retentionPolicy, point)
    }

    file.renameTo(new File("${file.name}.processed"))
  }

  /**
   * Line of the form
   * 2019-07-25 04:51:47.343 ,#730,digitalCommsEPAPool
   * @param file
   */
  void loadErrorsFile(File file) {
    loadErrors(file, "route_errors")
  }

  /**
   * Line of the form
   * 2019-09-05 00:01:13,502 ,org.springframework.scheduling.quartz.SchedulerFactoryBean#0_Worker-6
   * @param file
   */
  void loadErrorsFile(String app, File file) {
    loadErrors(file, "${app}_errors")
  }

  void loadErrors(File file, String measurement) {
    log.info("Loading [$file] into measurament [$measurement]")
    List<String> lines = file.readLines()
    lines.each { String line ->
      String[] values = line.split(",")
      String timeAsString = values[0].trim().replace(" ", "T")
      LocalDateTime eventTimestamp = LocalDateTime.parse(timeAsString, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
      ZonedDateTime zonedDateTime = eventTimestamp.atZone(ZoneOffset.systemDefault())
      Long millis = zonedDateTime.toInstant().toEpochMilli()
      String thread = values[1].trim()
      String route
      if(values.length>2) {

        route = values[2].trim()
      } else {
        route = thread
        thread = "unknown"
      }

      log.info("Processing [${measurement}]:d [$eventTimestamp($millis),$thread,$route]")
      Point point = Point.measurement(measurement)
          .tag("route", route)
          .addField("thread", thread)
          .time(millis, TimeUnit.MILLISECONDS)
          .build()

      client.write(databaseName, retentionPolicy, point)
    }

    file.renameTo(new File("${file.name}.processed"))
  }

  void loadMatchedDataFromDb() {
    String query = """
select distinct mcf.datetime_created, mcf.number_of_entries, mcf.file_name, avg(to_number(to_char(mcf.datetime_created, 'yyyymmddhh24miss')) - to_number(to_char(cff.datetime_created, 'yyyymmddhh24miss'))) elapsed_seconds
  from matched_call mc
  join matched_call_file mcf on mcf.file_name = mc.file_name
  join calling_flow_file cff on cff.file_name = mc.calling_data_file_name
  join calling_flow cf on cf.file_name = cff.file_name
 where mcf.datetime_created > sysdate - ${databaseQueryDays}
 group by mcf.datetime_created, mcf.number_of_entries, mcf.file_name
 order by mcf.datetime_created desc
"""
    log.info("Loading Digital data matching data from DB")
    groovySql.rows(query).each {Map row ->

      if(!processed.contains(row)) {
        processed.add(row)
        log.info("Loading row: [${row}] into DB and adding to processed list")
        String timeAsString = row.datetime_created
        LocalDateTime eventTimestamp = LocalDateTime.parse(timeAsString, new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(DateTimeFormatter.ISO_LOCAL_DATE)
            .appendLiteral(' ')
            .append(DateTimeFormatter.ISO_LOCAL_TIME)
            .toFormatter(ResolverStyle.STRICT, IsoChronology.INSTANCE))

//      Query influxQuery = new Query("select * from digital_matched_audit where")
//      client.query(influxQuery)

        ZonedDateTime zonedDateTime = eventTimestamp.atZone(ZoneOffset.systemDefault())
        Long millis = zonedDateTime.toInstant().toEpochMilli()
        Long matched = Long.valueOf(row.number_of_entries as String)
        String fileName = row.file_name
        String matchingElapsedSeconds = row.elapsed_seconds
        log.info("Processing [$eventTimestamp($millis),$matched,$fileName]")
        Point point = Point.measurement("digital_matched_audit")
            .tag("file_name", fileName)
            .tag("matching_elapsed", matchingElapsedSeconds)
            .addField("matched", matched)
            .time(millis, TimeUnit.MILLISECONDS)
            .build()

        client.write(databaseName, retentionPolicy, point)
      }

    }
  }

  void loadInboundFromRedxDb() {
    String query = """
select upload_date, transaction_type, file_size
  from document
 where market = 'NEM'
   and direction = 'IN'
   and upload_date > sysdate - ${databaseQueryDays}
"""
    log.info("Loading Redx NEM in data from DB")
    redxGroovySql.rows(query).each {Map row ->

      if(!processed.contains(row)) {
        processed.add(row)
        log.info("Loading row: [${row}] into DB and adding to processed list")
        String timeAsString = row.upload_date
        LocalDateTime eventTimestamp = LocalDateTime.parse(timeAsString, new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(DateTimeFormatter.ISO_LOCAL_DATE)
            .appendLiteral(' ')
            .append(DateTimeFormatter.ISO_LOCAL_TIME)
            .toFormatter(ResolverStyle.STRICT, IsoChronology.INSTANCE))

        ZonedDateTime zonedDateTime = eventTimestamp.atZone(ZoneOffset.systemDefault())
        Long millis = zonedDateTime.toInstant().toEpochMilli()
        Long fileSize = Long.valueOf(row.file_size as String)
        String transactionType = row.transaction_type
        log.info("Processing [$eventTimestamp($millis),$fileSize,$transactionType]")
        Point point = Point.measurement("redx_nem_inbound")
            .tag("transaction_type", transactionType)
            .addField("file_size", fileSize)
            .time(millis, TimeUnit.MILLISECONDS)
            .build()

        client.write(databaseName, retentionPolicy, point)
      }

    }
  }
}