package au.com.redenergy.loader.influxdb

import com.zaxxer.hikari.HikariDataSource
import groovy.sql.Sql
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.influxdb.InfluxDB
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile

import javax.annotation.Resource
import javax.sql.DataSource

@SpringBootApplication
@Slf4j
@Profile("default")
@CompileStatic 
class Application implements CommandLineRunner{
  @Autowired
  ApplicationContext context
  @Resource
  DataSource gentrackDataSource

  @Resource
  DataSource redxDataSource

  @Resource
  InfluxRedRouterLoader dataLoader

  static void main(String[] args) {
    SpringApplication.run(Application, args)
  }

  @Override
  void run(String... arg0) throws Exception {
    InfluxDB client = dataLoader.createClient()
//    client.createRetentionPolicy(dataLoader.retentionPolicy, dataLoader.databaseName, "30d", "30m", 2, true)
    client.enableBatch()
    while(true) {
      log.info("Checking if there is data to load")
      dataLoader.run()
      Thread.sleep(20000)
    }

  }

  @Bean
  @Primary
  @ConfigurationProperties(prefix = "gentrack.datasource")
  DataSourceProperties gentrackDataSourceProperties() {
    return new DataSourceProperties()
  }

  @Bean
  @ConfigurationProperties(prefix = "redx.datasource")
  DataSourceProperties redxDataSourceProperties() {
    return new DataSourceProperties()
  }

  @Bean(name = "gentrackDataSource")
  @Primary
  @ConfigurationProperties(prefix = "gentrack.datasource.configuration")
  HikariDataSource gentrackDataSource() {
    log.info("Got: ${gentrackDataSourceProperties().url}, ${gentrackDataSourceProperties().username}")
    return gentrackDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource).build()
  }

  @Bean(name = "redxDataSource")
  @ConfigurationProperties(prefix = "redx.datasource.configuration")
  HikariDataSource redxDataSource() {
    log.info("Got: ${redxDataSourceProperties().url}, ${redxDataSourceProperties().username}")
    return redxDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource).build()
  }

  @Bean
  Sql groovySql() {
    return new Sql(gentrackDataSource)
  }

  @Bean
  Sql redxGroovySql() {
    return new Sql(redxDataSource)
  }

}